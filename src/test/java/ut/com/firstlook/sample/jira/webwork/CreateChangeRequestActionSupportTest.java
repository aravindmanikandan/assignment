package ut.com.firstlook.sample.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.firstlook.sample.jira.webwork.CreateChangeRequestActionSupport;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class CreateChangeRequestActionSupportTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //CreateChangeRequestActionSupport testClass = new CreateChangeRequestActionSupport();

        throw new Exception("CreateChangeRequestActionSupport has no tests!");

    }

}
