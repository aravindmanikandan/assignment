package com.firstlook.sample.jira.entity.ao;

 import net.java.ao.Entity;
 import net.java.ao.ManyToMany;
 import net.java.ao.OneToMany;
 import net.java.ao.Preload;

@Preload
public interface CustomerEntity extends Entity
{


    String getCustName();
	void setCustName(String custName);

	String getGender();
	void setGender(String gender);


    String getCustDesc();
	void setCustDesc(String custDesc);

    String getEmailId();
	void setEmailId(String email_id);
    
}