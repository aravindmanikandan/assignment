package com.firstlook.sample.jira.service;

import com.firstlook.sample.jira.entity.ao.CustomerEntity;
import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.*;

public interface MappingService
{
	public  ActiveObjects getActiveObjects();
	public CustomerEntity addCustomerEntity(String name,String gender,String description,String email_id);
	public CustomerEntity updateCustomerEntity(int customerId,String name,String gender,String description,String email_id);
	public void deleteCustomerEntity(CustomerEntity customerentity);
	public Iterable<CustomerEntity> allCustomerEntity();
	public CustomerEntity getCustomerEntity(int id);
	public void deleteCustomerEntitys(int id);
}