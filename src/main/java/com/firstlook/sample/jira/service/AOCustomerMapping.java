package com.firstlook.sample.jira.service;

import com.firstlook.sample.jira.entity.ao.CustomerEntity;

import com.google.common.collect.Lists;
import net.java.ao.EntityManager;
import net.java.ao.Query;
import com.atlassian.activeobjects.external.ActiveObjects;
import org.springframework.util.StringUtils;
import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.stereotype.Component;
import java.sql.SQLException;
import java.util.Calendar;
import javax.annotation.concurrent.GuardedBy;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.EnumSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

import static com.google.common.base.Preconditions.*;


@ExportAsService
@Component
public final class AOCustomerMapping implements MappingService
{
	@ComponentImport
    protected final ActiveObjects activeObjects;
    private static final Logger log;
    
    static 
    {
        log = LoggerFactory.getLogger(AOCustomerMapping.class);
    }

    @Inject
	public AOCustomerMapping(ActiveObjects activeObjects)
    {
        this.activeObjects = checkNotNull(activeObjects);
    }


    // overriding Mapping Service methods

    @Override
    public ActiveObjects getActiveObjects()
    {
        return this.activeObjects;
    }


    @Override
    public CustomerEntity addCustomerEntity(String custname,String gender,String custdesc,String email_id)
    {
        final CustomerEntity cust_entity;
        try
        {
            cust_entity=activeObjects.create(CustomerEntity.class);
            cust_entity.setCustName(custname);
            cust_entity.setGender(gender);
            cust_entity.setCustDesc(custdesc);
            cust_entity.setEmailId(email_id);
            cust_entity.save();
            return cust_entity;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e); 

        }

    }

    @Override
    public CustomerEntity updateCustomerEntity(int customerId, String custname,String gender,String custdesc,String email_id)
    {
        final CustomerEntity cust_entity;
        try
        {
            cust_entity=getCustomerEntity(customerId);
            cust_entity.setCustName(custname);
            cust_entity.setGender(gender);
            cust_entity.setCustDesc(custdesc);
            cust_entity.setEmailId(email_id);
            cust_entity.save();

            return cust_entity;
        }
        catch(Exception e)
        {
            throw new RuntimeException(e); 
        }
    }

    @Override
    public Iterable<CustomerEntity> allCustomerEntity()
    {
        try
        {
            final Query query=Query.select().order("id ASC");
            
            return Lists.newArrayList(activeObjects.find(CustomerEntity.class,query));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

  
    

    @Override
    public void deleteCustomerEntity(CustomerEntity customerentity)
    {
        try
        {
            activeObjects.delete(customerentity);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }


    }


    @Override
    public void deleteCustomerEntitys(int customerId)
    {
        try
        {
            
            final Query query = Query.select()
                                .join(CustomerEntity.class,"FM.customerId = SC.ID")
                                .alias(CustomerEntity.class, "SC")
                                .alias(CustomerEntity.class, "FM")
                                .where("SC.ID = ?", customerId);

            Iterable<CustomerEntity> customerEntitys = Lists.newArrayList(activeObjects.find(CustomerEntity.class, query)); 
            for(CustomerEntity fieldMap : customerEntitys)
            {
                activeObjects.delete(fieldMap);    
            }

        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CustomerEntity getCustomerEntity(int id)
    {
        final CustomerEntity[] cust_entity;
        try
        {
            final Query query=Query.select().where("id = ?", id);
            cust_entity=activeObjects.find(CustomerEntity.class,query);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return cust_entity.length > 0 ? cust_entity[0] : null;

    }


}
