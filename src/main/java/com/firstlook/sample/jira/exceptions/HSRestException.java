package com.example.tutorial.plugins.exceptions;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HSRestException extends Exception {
    private final Map<String, Object> values;
    private List<String> errorMessages = Collections.emptyList();

    public HSRestException(String message) {
        this(message, null);
    }

    public HSRestException(String message, Throwable cause) {
        super(cause);
        values = new HashMap<String, Object>();
        errorMessages = Collections.singletonList(message);
    }

    public HSRestException(Map<String, Object> values) {
        this.values = values;
        Object errorMessageObj = values.get("errorMessages");
        if (errorMessageObj instanceof List) {
            errorMessages = (List<String>) errorMessageObj;
        }
    }

    public Map<String, Object> getValues() {
        return values;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public String getMessage() {
        return errorMessages.toString();
    }
}