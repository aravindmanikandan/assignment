
package com.example.tutorial.plugins.exceptions;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.*;

public class HSJSONNotFormattedException extends JSONException {
        private final String jsonString;

        public HSJSONNotFormattedException(final String message, final String jsonString) {
            super(message);
            this.jsonString = jsonString;
        }

        public String getJSONString() {
            return jsonString;
        }
    }