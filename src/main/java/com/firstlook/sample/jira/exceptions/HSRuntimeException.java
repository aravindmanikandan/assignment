
package com.example.tutorial.plugins.exceptions;

public class HSRuntimeException extends RuntimeException
{
    public HSRuntimeException()
    {
        super();
    }

    public HSRuntimeException(String message)
    {
        super(message);
    }

    public HSRuntimeException(String message, Throwable cause)
    {
        super(message, cause);
    }
}