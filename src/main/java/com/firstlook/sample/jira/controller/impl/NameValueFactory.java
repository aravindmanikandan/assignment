package com.example.tutorial.plugins.admin.controller.impl;
import com.example.tutorial.plugins.admin.controller.api.NameValueMapper;
import java.util.Comparator; 

public class NameValueFactory implements NameValueMapper
{
	public String name;
	public String value;


	public NameValueFactory(String name,String value)
	{
		this.name = name;
		this.value = value;
	}

	public String getName()
	{
		return name;
	}
	public String getValue()
	{
		return value;
	}
	public static Comparator<NameValueMapper> NameComparator = new Comparator<NameValueMapper>() {

	public int compare(NameValueMapper name1, NameValueMapper name2) {
	   String columnName1 = name1.getName().toUpperCase();
	   String columnName2 = name2.getName().toUpperCase();
	   //ascending order
	   return columnName1.compareTo(columnName2);
    }};

}