package com.firstlook.sample.jira.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 import javax.servlet.http.HttpServletRequest;
 import javax.ws.rs.Consumes;
 import javax.ws.rs.GET;
 import javax.ws.rs.POST;
 import javax.ws.rs.DELETE;
 import javax.ws.rs.PUT;
 import javax.ws.rs.Path;
 import javax.ws.rs.Produces;
 import javax.ws.rs.core.Context;
 import javax.ws.rs.core.Response.Status;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
 import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
 import com.atlassian.jira.component.ComponentAccessor;
 import com.atlassian.jira.security.JiraAuthenticationContext;
 import com.atlassian.jira.web.action.JiraWebActionSupport;
 import com.atlassian.jira.user.ApplicationUser;
 import java.util.ArrayList; 
 import java.util.List;
 import java.util.*;
 import java.util.Set;
 import javax.inject.Inject;
 import javax.inject.Named;
 import com.atlassian.sal.api.net.RequestFactory; 
 import com.atlassian.sal.api.user.*;
 import com.atlassian.sal.api.auth.LoginUriProvider;
 import com.atlassian.sal.api.pluginsettings.PluginSettings;
 import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
 import com.atlassian.sal.api.user.UserManager;
 import com.atlassian.sal.api.transaction.TransactionCallback;
 import com.atlassian.sal.api.transaction.TransactionTemplate;
 import com.firstlook.sample.jira.entity.ao.CustomerEntity;
 import com.firstlook.sample.jira.service.AOCustomerMapping;
 import com.firstlook.sample.jira.controller.XmlElement.XMLCustomer;


/**
 * A resource of message.
 */
@Path("/config")
@Named("RestResource")
public class RestResource 
{
	@ComponentImport
    private final UserManager userManager;
  @ComponentImport
    private final RequestFactory requestFactory; 
  @ComponentImport
    private JiraAuthenticationContext jiraAuthContext;
    private ApplicationUser currentAppUser;
  @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
	@ComponentImport
    private final TransactionTemplate transactionTemplate;
    private AOCustomerMapping aocustomermapping;
    
    private static final Logger log;
    static {
        log = LoggerFactory.getLogger(RestResource.class);
    }

    @Inject
  	public RestResource(UserManager userManager, PluginSettingsFactory pluginSettingsFactory,JiraAuthenticationContext jiraAuthContext,
        RequestFactory requestFactory,TransactionTemplate transactionTemplate, AOCustomerMapping aocustomermapping)
        
  	{
        this.userManager = userManager;
        this.jiraAuthContext = jiraAuthContext;
        this.requestFactory = requestFactory;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.currentAppUser = this.jiraAuthContext.getUser();
        this.transactionTemplate = transactionTemplate;
        this.aocustomermapping = aocustomermapping;
        
  	}

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/customervalues")
    public Response addCustomerEntity(final XMLCustomer xmlcustomer)
    {
        // String username= userManager.getRemoteUsername();
        // if(username == null || !userManager.isSystemAdmin(username))
        //     {
        //         return Response.status(Status.UNAUTHORIZED).build();
        //     }
        return Response.ok(transactionTemplate.execute(new TransactionCallback()
        {
               public Object doInTransaction()
                {
                try
                {
                    CustomerEntity customerentity = aocustomermapping.addCustomerEntity(xmlcustomer.custName,xmlcustomer.gender,xmlcustomer.custDesc,xmlcustomer.emailId);
                    return loadCustomerIntoXML(customerentity);
                }
                catch(Exception e)
                {
                    throw new RuntimeException(e);
                }
                }
        })).build();
    }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/customervalues")
  public Response getCustomerEntity()
  {

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              Iterable<CustomerEntity> schemeVariables = aocustomermapping.allCustomerEntity();
             return loadCustomerListIntoXML(schemeVariables);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/customervalue")
  public Response getCustomerEntitys()
  {

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }
    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
          {
              Iterable<CustomerEntity> schemeVariables = aocustomermapping.allCustomerEntity();
             return loadCustomerListIntoXML(schemeVariables);
          }
          catch(Exception e)
          {
              throw new RuntimeException(e);
          }
      }
    })).build();
  }
private List<XMLCustomer> loadCustomerListIntoXML(Iterable<CustomerEntity> customerVariables)
  {
        List<XMLCustomer> xmlCustomerList = new ArrayList<XMLCustomer>(); 
        for(CustomerEntity customerentity : customerVariables) {
          xmlCustomerList.add(loadCustomerIntoXML(customerentity));
        }
        return xmlCustomerList;
  }


    private XMLCustomer loadCustomerIntoXML(CustomerEntity customerentity)
    {
           XMLCustomer xmlcustomer = new XMLCustomer();
           xmlcustomer.id = customerentity.getID();
           xmlcustomer.custName = customerentity.getCustName();
           xmlcustomer.gender = customerentity.getGender();
           xmlcustomer.custDesc = customerentity.getCustDesc();
           xmlcustomer.emailId = customerentity.getEmailId();
           
           return xmlcustomer;
    }


  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/customervalue/{id}")
  public Response UpdateCustomerEntity(@PathParam("id") final int customerId,final XMLCustomer xmlcustomer)
  {

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }

    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
        {
            
           CustomerEntity customerentity = aocustomermapping.updateCustomerEntity(xmlcustomer.id,xmlcustomer.custName,xmlcustomer.gender,xmlcustomer.custDesc,xmlcustomer.emailId);
           
            return loadCustomerIntoXML(customerentity);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
      }
    })).build();
  }




  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/customervalue/{id}")
  public Response UpdateCustomerEntity(@PathParam("id") final int xmlSchemeID)
  {

    // String username = userManager.getRemoteUsername();
    //   if (username == null || !userManager.isSystemAdmin(username))
    //   {
    //     return Response.status(Status.UNAUTHORIZED).build();
    //   }

    return Response.ok(transactionTemplate.execute(new TransactionCallback()
    {
        public Object doInTransaction()
        {
        try
        {

           CustomerEntity customerentity = aocustomermapping.getCustomerEntity(xmlSchemeID);
       
            return loadCustomerIntoXML(customerentity);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
      }
    })).build();
  }




    @DELETE
    @Path("/customervalues/{id}")
    @Produces("text/plain")
    public Response deleteCustomerEntity( @PathParam("id") final int schemeID)
    {

      // String username = userManager.getRemoteUsername();
      // if (username == null || !userManager.isSystemAdmin(username))
      // {
      //   return Response.status(Status.UNAUTHORIZED).build();
      // }
      transactionTemplate.execute(new TransactionCallback()
      {
        @Override
        public Object doInTransaction()
        {
        try
        {
          CustomerEntity cust = aocustomermapping.getCustomerEntity(schemeID);
          aocustomermapping.deleteCustomerEntity(cust);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
        return null;
      }
    });
    return Response.noContent().build();
  }

}   
